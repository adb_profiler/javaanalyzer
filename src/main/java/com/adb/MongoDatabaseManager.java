package com.adb;

import com.adb.entity.Configuration;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

/**
 * 
 * @author P.Kostrzewa
 *
 */
public class MongoDatabaseManager {
	
	/** */
	private static MongoClient mongoClient;
	/** */
	private static MongoDatabaseManager INSTANCE;
	
	public static MongoDatabaseManager getInstance() 
	{
		if (INSTANCE == null)
		{
			INSTANCE = new MongoDatabaseManager();
		}
		return INSTANCE;
	}

	/** */
	private MongoDatabaseManager() 
	{
		mongoClient = new MongoClient(Configuration.getInstance().getDatabaseHost());
	}
	
	/**
	 * 
	 * @param dbName
	 * @return
	 */
	public MongoDatabase getDatabase(final String dbName) 
	{
		return mongoClient.getDatabase(dbName);
	}

	
}
