package com.adb;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javassist.ClassPath;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;

/**
 * 
 * @author S.Perek
 *
 */
public class ClassesCollector {
	public static ClassPool pool;
	public static ClassPath path;
	public static String jarFileLocalization;
	
	private List<CtClass> ClassesList = new ArrayList<CtClass>();

	public List<CtClass> getClassesList() {
		return ClassesList;
	}

	private HashMap<String, ZipEntry> classNames = new HashMap<String, ZipEntry>();
	
	
	ZipInputStream zip = null;
	
	public ClassesCollector(String jarFileLocalization) {
		pool = ClassPool.getDefault();
		try {
			ClassesCollector.path = pool.insertClassPath(jarFileLocalization);
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
		ClassesCollector.jarFileLocalization = jarFileLocalization;
		getNamesFromJarFile();
		addJarClasses();
		
	}
	
	public void getNamesFromJarFile() {
		try {
			zip = new ZipInputStream(new FileInputStream(jarFileLocalization));
			for (ZipEntry entry = zip.getNextEntry(); entry != null; entry = zip.getNextEntry()) {
				if (!entry.isDirectory() && entry.getName().endsWith(".class")) {
					final String className = entry.getName().replaceAll("/", ".").replace(".class", ""); // including
					classNames.put(className, entry);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void addJarClasses() {
		System.out.println(classNames.toString());
		for (String name : classNames.keySet()) {
			try {
				ClassesList.add(pool.get(name));
			} catch (NotFoundException e) {
				e.printStackTrace();
			}
		}
	}
}
