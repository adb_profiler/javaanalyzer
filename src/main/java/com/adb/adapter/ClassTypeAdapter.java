package com.adb.adapter;

import java.lang.reflect.Type;

import com.adb.entity.Class;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * 
 * @author P.Kostrzewa
 *
 */
public class ClassTypeAdapter implements JsonSerializer<Class> {

	@Override
	public JsonElement serialize(Class src, Type typeOfSrc, JsonSerializationContext context) {
		JsonObject json = new JsonObject();
		json.addProperty("package", src.getPackageName());
		json.addProperty("class", src.getName());
		json.add("version", context.serialize(src.getVersion()));
		json.add("methods", context.serialize(src.getDocuments()));
		return json;
	}
	
}
