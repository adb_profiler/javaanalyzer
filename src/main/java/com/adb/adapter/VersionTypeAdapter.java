package com.adb.adapter;

import java.lang.reflect.Type;

import com.adb.entity.Version;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * 
 * @author P.Kostrzewa
 *
 */
public class VersionTypeAdapter implements JsonSerializer<Version> {

	@Override
	public JsonElement serialize(Version src, Type typeOfSrc, JsonSerializationContext context) {
		JsonObject json = new JsonObject();
		
		json.addProperty("commit_id", src.getCommitId());
		json.addProperty("branch", src.getBranch());
		json.addProperty("commited_at", src.getDate());
		
		return json;
	}

}
