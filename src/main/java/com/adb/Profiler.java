package com.adb;
import java.io.IOException;

import com.adb.controller.AnalyzerController;
import com.adb.controller.BuildController;
import com.adb.controller.ConfigurationController;
import com.adb.controller.GitController;

public class Profiler {
	
	/**
	 * @author P.Kostrzewa
	 * 
	 * @param args
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws IOException, InterruptedException {
		
			long start = System.currentTimeMillis();
			
			ConfigurationController configurationService = new ConfigurationController();
			GitController gitService = new GitController();
			BuildController buildService = new BuildController();
			AnalyzerController analyzerService = new AnalyzerController();
			
			configurationService.init();
			gitService.cloneRepositories();
			buildService.buildRepositories();
			analyzerService.analyzeJarFiles();
			
			long end = System.currentTimeMillis() - start;
			
			System.out.println("\n\n\n\n FINISHED IN:" + end);
			
	}
}


