package com.adb.controller;

import com.adb.entity.Method;
import com.google.gson.Gson;

/**
 * 
 * @author P.Kostrzewa
 *
 */
public class MethodController {
	
	private Method method = null;
	/** */
	private Gson gson = new Gson();
	
	public MethodController(Method method) {
		this.method = method;
	}
	
	public MethodController() {
		
	}
	
	public String toJson() {
		return this.gson.toJson(this.method);
	}
	
	public Method fromJson(String json) {
		return this.gson.fromJson(json, Method.class);	
	}
}
