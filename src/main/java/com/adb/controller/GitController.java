package com.adb.controller;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ListBranchCommand.ListMode;
import org.eclipse.jgit.api.TransportConfigCallback;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig;
import org.eclipse.jgit.transport.SshSessionFactory;
import org.eclipse.jgit.transport.SshTransport;
import org.eclipse.jgit.transport.Transport;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import com.adb.entity.Configuration;
import com.jcraft.jsch.Session;

/**
 * 
 * @author P.Kostrzewa
 *
 */
public class GitController {
	
	private final Configuration configurationInstance = Configuration.getInstance();
	private final ConfigurationController configurationService = new ConfigurationController();
			
	public static Git git = null;
	public static List<String> directories = new ArrayList<>();
	
	public void cloneRepositories() {
		List<String> versions = configurationInstance.getVersion();
		
		for(String version : versions) {
			cloneRemoteRepository();
			branchExists();
			
			if(configurationService.isValidDate(version)) {
				checkoutByDate(version);
			} else {
				checkoutByHash(version);
			}
		}
	}
	
	public void checkoutByDate(String version) {
		
		try {
			Iterable<RevCommit> commits = GitController.git.log().all().call();
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			
			for(RevCommit commit : commits) {
				Date when = commit.getAuthorIdent().getWhen();
				String dateFormatted = dateFormat.format(when);
				
				if(version.equals(dateFormatted)) {
					GitController.git.checkout().setCreateBranch(true).setName(commit.getName()).setStartPoint(commit).call();
					break;
				}
				
			}
			
		} catch (GitAPIException | IOException e) {
			e.printStackTrace();
		}
	}
	
	public void checkoutByHash(String version) {

		try {
			Iterable<RevCommit> commits = GitController.git.log().all().call();
			
			for(RevCommit commit : commits) {
				
				if(version.equals(commit.getName())) {
					GitController.git.checkout().setCreateBranch(true).setName(commit.getName()).setStartPoint(commit).call();
					break;
				}
				
			}
			
		} catch (GitAPIException | IOException e) {
			e.printStackTrace();
		}
	}
	
	private void cloneRemoteRepository() {	
		CloneCommand cloneCommand = new CloneCommand();
		Git result = null;
		
		switch(configurationInstance.getAuthType()) {
			case "https": 
				cloneCommand = httpsAuth(); 
				break;
			case "ssh":
				cloneCommand = sshAuth();
				break;
			default:
				cloneCommand = httpsAuth();
				break;
		}
		
		try {
			result = cloneCommand.call();
		} catch (GitAPIException e) {
			e.printStackTrace();
		} finally {
			GitController.git = new Git(result.getRepository());
		}
	}
	
	public List<Ref> getAvailableBranches() {
		List<Ref> branches = new ArrayList<>();
		try {
			return GitController.git.branchList().setListMode(ListMode.ALL).call();
		} catch (GitAPIException e) {
			e.printStackTrace();
		}
		
		return branches;
	}
	
	public boolean branchExists() {
		String remoteRepositoryBranch = configurationInstance.getRemoteRepositoryBranch();
		
		for(Ref branch : getAvailableBranches()) {
			if(branch.getName().equals(remoteRepositoryBranch)) {
				return true;
			}
		}

		
		return false;
	}
	
	private CloneCommand httpsAuth() {
		
		File local = generateLocalRepositoryFolder();
			
		CloneCommand cloneCommand = Git.cloneRepository();
		cloneCommand.setURI(configurationInstance.getRemoteRepositoryUrl());
		cloneCommand.setDirectory(local);
		cloneCommand.setCredentialsProvider(new UsernamePasswordCredentialsProvider("user", "pwd"));
		
		return cloneCommand;
	}
	
	private CloneCommand sshAuth() {
		
		File local = generateLocalRepositoryFolder();
		
		JschConfigSessionFactory sshSessionFactory = new JschConfigSessionFactory() {
			@Override
			protected void configure(OpenSshConfig.Host hc, Session session) {
//				CredentialsProvider provider = new CredentialsProvider() {
//			        @Override
//			        public boolean isInteractive() {
//			            return false;
//			        }
//
//			        @Override
//			        public boolean supports(CredentialItem... items) {
//			            return true;
//			        }
//
//			        @Override
//			        public boolean get(URIish uri, CredentialItem... items) throws UnsupportedCredentialItem {
//			            for (CredentialItem item : items) {
//			                ((CredentialItem.StringType) item).setValue("");
//			            }
//			            return true;
//			        }
//			    };
//			    
//			    UserInfo userInfo = new CredentialsProviderUserInfo(session, provider);
//			    session.setUserInfo(userInfo);
			}
		};
		
		SshSessionFactory.setInstance(sshSessionFactory);
		
		CloneCommand cloneCommand = Git.cloneRepository();
		cloneCommand.setURI(configurationInstance.getRemoteRepositoryUrl());
		cloneCommand.setDirectory(local);
		cloneCommand.setTransportConfigCallback(new TransportConfigCallback() {
			@Override
			public void configure(Transport transport) {
				SshTransport sshTransport = (SshTransport) transport;
				sshTransport.setSshSessionFactory(sshSessionFactory);
			}
		});
		
		return cloneCommand;
	}
	
	private File generateLocalRepositoryFolder() {
		String randomString = UUID.randomUUID().toString().replace("-", "");
		String filePath = System.getProperty("user.home") + File.separator + "repositories" + File.separator + "repository_" + randomString;
		
		directories.add("repository_" + randomString);
		
		return new File(filePath);
	}
	
}
