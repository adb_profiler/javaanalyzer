package com.adb.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * 
 * @author P.Kostrzewa
 *
 */
public class BuildController 
{

	/**
	 * 
	 */
	public void buildRepositories() {
		for(String directory : GitController.directories) {
			buildGradleProject(directory);
		}
	}
	
	/**
	 * 
	 * @param directory
	 */
	public void buildGradleProject(String directory) {
		
		String path = System.getProperty("user.home") + "/repositories/" + directory;
		
		ProcessBuilder builder = new ProcessBuilder("gradle", "build").directory(new File(path));
		
		try {
			Process process = builder.start();
			process.waitFor();
	
			final StringBuilder stringBuilder = new StringBuilder();
			final BufferedReader bufferedReader;
			
			InputStream inputStream = process.getInputStream();
			bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			
			String line = null;
			while((line = bufferedReader.readLine()) != null) {
				stringBuilder.append(line + System.getProperty("line.separator"));
			}
			System.out.println(stringBuilder);
			bufferedReader.close();
			
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
			
	}
}
