package com.adb.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.bson.Document;

import com.adb.MongoDatabaseManager;
import com.adb.adapter.VersionTypeAdapter;
import com.adb.entity.Configuration;
import com.adb.entity.Version;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mongodb.client.MongoDatabase;

/**
 * 
 * @author P.Kostrzewa
 *
 */
public class VersionController {
	
	private static final String COLLECTION_NAME = "version";
	private final MongoDatabase db = MongoDatabaseManager.getInstance().getDatabase(Configuration.getInstance().getDatabaseName());
	
	private Version version = null;
	private Gson gson = new GsonBuilder().registerTypeAdapter(Version.class, new VersionTypeAdapter()).excludeFieldsWithoutExposeAnnotation().create();
	
	
	public VersionController(Version version) {
		this.version = version;
	}
	
	public VersionController() {
		
	}
	
	public String toJson() {
		return this.gson.toJson(this.version);
	}
	
	public Version fromJson(String json) {
		return this.gson.fromJson(json, Version.class);
	}
	
	public void removeAll() {
		db.getCollection(COLLECTION_NAME).drop();
	}
	
	public Document generateDocument() {
		return Document.parse(this.toJson());
	}
	
	public String convertDateToString(Date commited_at) {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return dateFormat.format(commited_at);
		
	}
	
}
