package com.adb.controller;

import org.bson.Document;

import com.adb.MongoDatabaseManager;
import com.adb.adapter.ClassTypeAdapter;
import com.adb.adapter.VersionTypeAdapter;
import com.adb.entity.Class;
import com.adb.entity.Configuration;
import com.adb.entity.Method;
import com.adb.entity.Version;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mongodb.client.MongoDatabase;

import javassist.CannotCompileException;
import javassist.CtMethod;
import javassist.bytecode.BadBytecode;
import javassist.bytecode.CodeIterator;
import javassist.bytecode.MethodInfo;

/**
 * 
 * @author P.Kostrzewa
 *
 */
public class ClassController {
	
	/** */
	private static final String COLLECTION_NAME = "classes";
	/** */
	private final MongoDatabase db = MongoDatabaseManager.getInstance().getDatabase(Configuration.getInstance().getDatabaseName());
	/** */
	private final Class clazz;
	/** */
	private Gson gson = new GsonBuilder().registerTypeAdapter(Class.class, new ClassTypeAdapter()).registerTypeAdapter(Version.class, new VersionTypeAdapter()).excludeFieldsWithoutExposeAnnotation().create();	
	
	/**
	 * @param clazz
	 */
	public ClassController(Class clazz) 
	{
		this.clazz = clazz;
	}
	
	public String toJson() 
	{
		return this.gson.toJson(clazz, Class.class);
	}
	
	public Class fromJson(String json) 
	{
		return this.gson.fromJson(json, Class.class);
	}
	
	public void insert() 
	{
		Document document = Document.parse(toJson());
		db.getCollection(COLLECTION_NAME).insertOne(document);
	}
	
	public void parse() 
	{
		
		if(this.clazz != null) {
			this.parseClassMethods();
			if(validatePackageName(this.clazz.getCc().getPackageName())) {
				this.insert();
			}
		} else {
			System.out.println("SKIPPING");
		}
			
	}
	
	/**
	 * 
	 */
	private void parseClassMethods() 
	{
		if(validatePackageName(this.clazz.getCc().getPackageName())) {
			System.out.println(this.clazz.getCc().getPackageName().toUpperCase() + " CLASS: " + this.clazz.getName());
			
			for (final CtMethod method : this.clazz.getMethods()) 
			{
				try 
				{	
					
					this.clazz.getCc().defrost();
					
					if(!method.isEmpty()) 
					{
						method.insertBefore("{ System.out.println(\"Entering: " + method.getLongName() + " Current timestamp: \" + System.currentTimeMillis());}");
						method.insertAfter("{ System.out.println(\"Leaving: " + method.getLongName() + " Current timestamp:\" + System.currentTimeMillis());}", true);
					}
			
				} 
				catch (CannotCompileException e) 
				{
					e.printStackTrace();
				}
				
				final long countedBytecodeInstructions = countBytecodeInstructions(method.getMethodInfo());
				final Method m = new Method(method.getSignature(), method.getName(), countedBytecodeInstructions, clazz.getPackageName(), clazz.getName());
				this.clazz.addDocument(m);
			}
			
		}
		final ArchiveController archiveService = new ArchiveController();
		archiveService.addEntry(clazz.getCc());		
	}
	
	/**
	 * 
	 * @param methodInfo
	 * @return Number of bytecode in method
	 */
	private long countBytecodeInstructions(MethodInfo methodInfo) 
	{
		long countedInstructions = 0;
		final CodeIterator codeIterator;
		
		if (methodInfo.getCodeAttribute() != null) {
			codeIterator = methodInfo.getCodeAttribute().iterator();
			
			while(codeIterator.hasNext()) {
				countedInstructions++;
				try {
					codeIterator.next();
				} catch (BadBytecode e) {
					e.printStackTrace();
				}
			}
		}
		return countedInstructions;
	}
	
	private boolean validatePackageName(String packageName) {
		
		final Configuration configurationInstance = Configuration.getInstance();
		boolean matches = false;
		
		for(String singlePackage : configurationInstance.getPackage()) {
			if(packageName.matches(".*"+ singlePackage +".*")) {
				matches = true;
			}
		}
		
		return matches;
	}
	
}
