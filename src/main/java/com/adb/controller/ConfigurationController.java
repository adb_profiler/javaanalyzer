package com.adb.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import com.adb.entity.Configuration;

/**
 * 
 * @author P.Kostrzewa
 *
 */
public class ConfigurationController {
	
	Properties properties = new Properties();
	
	public void init() {
		InputStream inStream = null;
		
		try {
			inStream = new FileInputStream("/home/P.Kostrzewa/Desktop/config.properties");
			properties.load(inStream);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			loadConfigurationFromFile();
		}
	}
	
	private void loadConfigurationFromFile() 
	{
		Configuration configurationInstance = Configuration.getInstance();
		
		configurationInstance.setDatabaseName(properties.getProperty("db_name"));
		configurationInstance.setDatabaseHost(properties.getProperty("db_host"));
		
		configurationInstance.setRemoteRepositoryUrl(properties.getProperty("remote_repository_url"));
		configurationInstance.setRemoteRepositoryBranch(properties.getProperty("remote_repository_branch"));
		configurationInstance.setAuthType(properties.getProperty("auth"));
		
		configurationInstance.setVersion(getVersionProperty());
		configurationInstance.setPackage(getPackageProperty());
		
	
	}
	
	private List<String> getVersionProperty() 
	{
		return new ArrayList<String>(Arrays.asList(properties.getProperty("version").split(",", -1)));
	}
	
	private List<String> getPackageProperty() 
	{
		return new ArrayList<String>(Arrays.asList(properties.getProperty("package").split(",", -1)));
	}

	public boolean isValidDate(String input)
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		if (input == null || input.trim().length() != dateFormat.toPattern().length()) {
			return false;
		}
		
		dateFormat.setLenient(false);
		      
		try {
			dateFormat.parse(input);
		} catch(ParseException e) {
			
		}
		
		return true;
	}
	
}
