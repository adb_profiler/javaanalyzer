package com.adb.controller;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;

import com.adb.entity.Archive;

import javassist.CannotCompileException;
import javassist.CtClass;

/**
 * 
 * @author P.Kostrzewa
 *
 */
public class ArchiveController 
{
	
	private static Set<String> alreadyAddedFiles = new HashSet<>();
	
	/**
	 * 
	 * @param clazz
	 * @throws IOException
	 */
	public void addEntry(final CtClass clazz) 
	{
		final JarOutputStream tempJar = AnalyzerController.tempJar;
//		System.out.println("addEntry: " + clazz.getName());
	
		final JarEntry entry = new JarEntry(clazz.getName().replace(".", "/") + ".class");
//		System.out.println("New Entry: " + entry);
		try {
			tempJar.putNextEntry(entry);
			clazz.toBytecode(new DataOutputStream(tempJar));
			alreadyAddedFiles.add(entry.getName());
			tempJar.closeEntry();
		} catch (IOException | CannotCompileException e) {
			e.printStackTrace();
		}

//		System.out.println(entry.getName());
		
	}
	
	public void addRemainingEntries() 
	{
		
		final JarOutputStream tempJar = AnalyzerController.tempJar;
		final Archive archive = AnalyzerController.archive;
		final JarFile jarFile;
		
		try 
		{
			jarFile = new JarFile(new File(archive.getAbsolutePath()));
			
			for(Enumeration<JarEntry> entries = jarFile.entries(); entries.hasMoreElements();)
			{
				
				JarEntry entry = entries.nextElement();
//				
//				if(!entry.getName().endsWith(".class"))
//				{
					int bytesRead = 0;
					byte[] buffer = new byte[1024];
					
					if(!alreadyAddedFiles.contains(entry.getName())) {
						tempJar.putNextEntry(entry);
						alreadyAddedFiles.add(entry.getName());
					
						InputStream in = jarFile.getInputStream(entry);
						
						while((bytesRead = in.read(buffer)) != -1)
						{
							tempJar.write(buffer, 0, bytesRead);
						}
						tempJar.closeEntry();
						
//						System.out.println(entry.getName());
					}
					
//				}
				
			}
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		

	}

}
