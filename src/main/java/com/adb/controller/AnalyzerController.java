package com.adb.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.jar.JarOutputStream;

import com.adb.ClassesCollector;
import com.adb.entity.Archive;
import com.adb.entity.Class;
import com.adb.entity.Configuration;

import javassist.CtClass;

/**
 * 
 * @author P.Kostrzewa
 *
 */
public class AnalyzerController 
{

	/** */
	public static Archive archive;
	/** */
	public static JarOutputStream tempJar;
	
	/**
	 * 
	 */
	public void analyzeJarFiles() 
	{
		
		for(final String directory : GitController.directories) 
		{

			try 
			{
				archive = new Archive(directory);
				analyzeJarFile(directory);
			} 
			catch (IOException e) 
			{
				
				e.printStackTrace();
			
			}
		}
		
	}
	
	/**
	 * 
	 * @param directory
	 * @throws IOException
	 */
	private void analyzeJarFile(String directory) throws IOException 
	{	
		
		final ClassesCollector classesCollector = new ClassesCollector(archive.getAbsolutePath());
		final List<CtClass> classesList = classesCollector.getClassesList();
		final ArchiveController archiveService = new ArchiveController();
		
		
		final File tempFile = new File(archive.getAbsolutePath() + ".tmp");
		tempJar = new JarOutputStream(new FileOutputStream(tempFile));
				
		for (final CtClass cc : classesList) 
		{
//			if(validatePackageName(cc.getPackageName())) {
				run(cc);
//			}
		}
		
		archiveService.addRemainingEntries();
		tempJar.close();
		
	}

	/**
	 * 
	 * @param cc
	 */
	public void run(CtClass cc) 
	{
		
		Class clazz = new Class(cc);
//		System.out.println("AnalyzerService.run: className = " + clazz.getName());
		ClassController classService = new ClassController(clazz);
		classService.parse();
		
	}
	
}
