package com.adb.entity;

import java.util.Arrays;

import com.google.gson.annotations.Expose;

import javassist.CtClass;
import javassist.CtConstructor;
import javassist.CtMethod;

/**
 * 
 * @author P.Kostrzewa
 *
 */
public class Class {
	
	@Expose
	private String packageName = null;
	@Expose
	private String name = null;
	@Expose
	private Version version = null;
	@Expose
	private Method[] methodDocuments = new Method[0];

	private CtConstructor[] constructors;
	private CtMethod[] methods;
	private CtClass[] nestedClasses;
	private CtClass[] interfaces;
	private CtClass cc;

	public Class(CtClass cc) {
		this.packageName = cc.getPackageName();
		this.name = cc.getName();
		this.constructors = cc.getConstructors();
		this.methods = cc.getDeclaredMethods();
		this.setCc(cc);
//		this.version = GitService.version;
			
//		try {
//			this.nestedClasses = cc.getNestedClasses();
//			this.interfaces = cc.getInterfaces();
//		} catch (NotFoundException e) {
//			e.printStackTrace();
//		}
	}
	
	public CtClass getCc() {
		return cc;
	}


	public void setCc(CtClass cc) {
		this.cc = cc;
	}
	
	public Version getVersion() {
		return version;
	}
	
	public String getPackageName() {
		return packageName;
	}

	public String getName() {
		return name;
	}

	public CtConstructor[] getConstructors() {
		return constructors;
	}

	public CtMethod[] getMethods() {
		return methods;
	}

	public CtClass[] getNestedClasses() {
		return nestedClasses;
	}

	public CtClass[] getInterfaces() {
		return interfaces;
	}

	public Method[] getDocuments() {
		return methodDocuments;
	}

	public void setDocuments(Method[] documents) {
		this.methodDocuments = documents;
	}
	
	public void addDocument(final Method m) 
	{
		methodDocuments = Arrays.copyOf(methodDocuments, methodDocuments.length + 1);
		methodDocuments[methodDocuments.length - 1] = m;
	}
	
	
}
