package com.adb.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author P.Kostrzewa
 *
 */
public class Configuration {
	
	private static Configuration INSTANCE;

	private String DB_NAME = null;
	private String DB_HOST = null;
	
	private String REMOTE_REPOSITORY_URL = null;
	private String REMOTE_REPOSITORY_BRANCH = null;
	private List<String> VERSION = new ArrayList<>();
	private String AUTH_TYPE = null;
	
	private List<String> PACKAGE = new ArrayList<>();
	
	public static Configuration getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Configuration();
		}
		
		return INSTANCE;
	}
	
	public String getDatabaseName() {
		return DB_NAME;
	}
	
	public void setDatabaseName(String databaseName) {
		this.DB_NAME = databaseName;
	}
	
	public String getDatabaseHost() {
		return DB_HOST;
	}
	
	public void setDatabaseHost(String databaseHost) {
		this.DB_HOST = databaseHost;
	}
	
	public String getRemoteRepositoryUrl() {
		return REMOTE_REPOSITORY_URL;
	}
	
	public void setRemoteRepositoryUrl(String remoteRepositoryUrl) {
		this.REMOTE_REPOSITORY_URL = remoteRepositoryUrl;
	}
	
	public String getRemoteRepositoryBranch() {
		return REMOTE_REPOSITORY_BRANCH;
	}
	
	public void setRemoteRepositoryBranch(String remoteRepositoryBranch) {
		this.REMOTE_REPOSITORY_BRANCH = remoteRepositoryBranch;
	}
	
	public List<String> getVersion() {
		return VERSION;
	}
	
	public void setVersion(List<String> version) {
		this.VERSION = version;
	}

	public String getAuthType() {
		return AUTH_TYPE;
	}

	public void setAuthType(String authType) {
		this.AUTH_TYPE = authType;
	}

	public final List<String> getPackage() {
		return PACKAGE;
	}

	public final void setPackage(List<String> packages) {
		this.PACKAGE = packages;
	}
	
}
