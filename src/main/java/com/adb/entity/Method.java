package com.adb.entity;

import com.google.gson.annotations.Expose;

/**
 * 
 * @author P.Kostrzewa
 *
 */
public class Method {
	
	@Expose
	private String signature;
	@Expose
	private long bytecodeInstructions;
	@Expose
	private String name;

	public Method(String signature, String name, long bytecodeInstructions, String packageName, String className) {
		this.name = name;
		this.bytecodeInstructions = bytecodeInstructions;
		this.signature = signature;
	}
	
	public String getSignature() {
		return signature;
	}

	public long getBytecodeInstructions() {
		return bytecodeInstructions;
	}

	public String getName() {
		return name;
	}
	
}
