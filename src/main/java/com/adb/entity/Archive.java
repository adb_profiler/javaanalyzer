package com.adb.entity;

import java.io.File;

/**
 * 
 * @author P.Kostrzewa
 *
 */
public class Archive 
{
	
	/** */
	private final String absolutePath;
	/** */
	private final String name;
	
	/**
	 * 
	 * @param name
	 */
	public Archive(String name) 
	{
		this.name = name;
		this.absolutePath = generateAbsolutePath(name);
//		System.out.println(absolutePath);
	}
	
	/**
	 * 
	 * @return Absolute path to the archive
	 */
	public final String getAbsolutePath() 
	{
		return absolutePath;
	}
	
	/**
	 * 
	 * @return Archive name
	 */
	public final String getName() 
	{
		return name;
	}
	
	/**
	 * 
	 * @param name
	 * @return 
	 */
	private String generateAbsolutePath(String name) 
	{
		String home = System.getProperty("user.home") + File.separator;
		String libs = "repositories/" + name + "/build/libs/";
		String jar = name + "-1.0.jar";
		return home + libs + jar;	
	}
	
}
