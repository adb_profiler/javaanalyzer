package com.adb.entity;

/**
 * 
 * @author P.Kostrzewa
 *
 */
public class Version
{

	private String commitId = "";

	private String branch = "";

	private String commitedAt = null;
	
	public Version() {
		
	}
	
	public String getCommitId() {
		return commitId;
	}
	
	public void setCommitId(String commitId) {
		this.commitId = commitId;
	}
	
	public String getBranch() {
		return branch;
	}
	
	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	public String getDate() {
		return commitedAt;
	}
	
	public void setDate(String date) {
		this.commitedAt = date;
	} 
	
	
}
